import Login from '../screens/login';
import Otp from '../screens/login/otp';
import SplashScreen from '../screens/splash';
import Home from '../screens/home/home';
import HomeInitial from '../screens/home';
import Landing from '../screens/landing';
import Register from '../screens/register';
import SignUpStage from '../screens/signUpStages';
import Loading from '../screens/loading';
import CommonScreen from '../screens/commonScreen';
import MyProfile from '../screens/myProfile';
import CommisssionDetail from '../screens/myCommission/commisionDetail';
import MyCommission from '../screens/myCommission';
import MyPolicy from '../screens/myPolicy';
import PolicyDetails from '../screens/myPolicy/policyDetail';
import MyQuote from '../screens/myQuote';
import GetQuote from '../screens/getQuote';
import ForgotPassword from '../screens/forgotpassword'
import CancelPolicy from '../screens/cancelpolicy'
import Payment from '../screens/getQuote/payment'

export default {
   SplashScreen,
   Login,
   Home,
   HomeInitial,
   Landing,
   Register,
   SignUpStage,
   Loading,
   CommonScreen,
   MyProfile,
   MyCommission,
   CommisssionDetail,
   MyPolicy,
   PolicyDetails,
   Otp,
   MyQuote,
   GetQuote,
   ForgotPassword,
   CancelPolicy,
   Payment
}

