import axios from 'axios';
import NetInfo from "@react-native-community/netinfo";
import ModalAlert from '../utils/modal'
import Config from 'react-native-config';
import { getFormatterError } from '../utils/getFormatterMessage'
import { Platform, AsyncStorage } from 'react-native';
import { getBaseUrl } from './index';
import { getToken } from '../redux/client'


async function validateToken() {
    let token = await AsyncStorage.getItem('token');
    return token;
}

const requestMultiPart = async function (options, raiseFullError = false) {

    const client = axios.create({
        baseURL: getBaseUrl(true),

        headers: {
            'Content-Type': "application/x-www-form-urlencoded",
            "Accept": "application/json",
            "device-type": Platform.OS,
            "Authorization": "Bearer " + await AsyncStorage.getItem('token')
        },
    });

    console.log("Base URL::::>", getBaseUrl(true))
    console.log("Request Data:::>", options, validateToken())


    const onSuccess = function (response) {
        console.log("Response Data Multipart:::>", response.data)
        if (response.data.status) {
            return Promise.resolve(response.data);
        } else {
            return Promise.reject(response);
        }
    };

    const onError = function (error) {
        console.log("Response Error Multipart:::>", error);
        if(error.data){
            return Promise.reject(error.data);
        }else{
            ModalAlert.error("Something went wrong")
        }
    };

    return NetInfo.fetch().then(state => {
        if (state.isConnected) {
            return client(options).then(onSuccess).catch(onError);
        } else {
            ModalAlert.error('Please check your internet connection.')
            return
        }
    });
};



export function responseLog(tag, baseUrl, options, response) {
    LogTracker.debug(tag, {
        baseUrl,
        options,
        response
    });
}


export function cancelAll(message) {
    source.cancel(message);
    source = axios.CancelToken.source();
}

// export default request;
export default requestMultiPart;


