
import React from 'react';
import { View, Image, Text, StatusBar, Animated, TouchableOpacity, SafeAreaView, StyleSheet, ScrollView } from 'react-native';
// import SignInNavigator from '../../navigation/SignInNavigator'

import { createAppContainer, createSwitchNavigator } from "react-navigation";

import { connect } from "react-redux";
import { createStackNavigator } from 'react-navigation-stack';
import ScreenName from '../../navigation/screenNames'
import colors from '../../utils/colors';
import TextInputComponent from '../../components/textInput'
import * as SSOServices from '../../services/SSOService'
import Modal from '../../utils/modal';


class MyProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isApp: false,
            userData:{

            }
        };
        this.springValue = new Animated.Value(1);
        this.fadeInOpacity = new Animated.Value(0);


    }

    componentDidMount() {
        this.getProfileDetails()
    }

    getProfileDetails = () =>{
        let userId = this.props.userData.user_id
        let modal = Modal.createProgressModal("Fetching data...")
        SSOServices.getProfileData(userId+"/1").then(res=>{
            Modal.hide(modal);
            this.setState({
                userData:res.data
            })
        }).then(res=>{
            Modal.hide(modal)
        })
    }




    render() {
        return (
            <View style={{flex: 1,marginBottom:'5%',marginTop:50}}>
                <View style={styles.toolbar}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <Image source={require('../../assets/images/arrow.png')}
                            style={[styles.logo]} />
                    </TouchableOpacity>
                    <Text style={styles.home}>Personal Details</Text>
                </View>

                <Text style={styles.profileDesc}>Profile once approved can not be changed please contact administrator at info@travelmedicare.com for making changes to profile</Text>

                <ScrollView>
                <TextInputComponent
                    isSecure={false}
                    placeholder={"First Name"}
                    maxLength={100}
                    value={this.state.userData.first_name}
                    disable={true}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"Last Name"}
                    maxLength={100}
                    value={this.state.userData.last_name}
                    disable={true}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"Email Address"}
                    value={this.state.userData.email}
                    maxLength={100}
                    disable={true}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"Website"}
                    value={this.state.userData.website}
                    maxLength={100}
                    disable={true}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"Phone"}
                    value={this.state.userData.phone}
                    disable={true}
                    maxLength={100}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"Address"}
                    value={this.state.userData.address}
                    maxLength={100}
                    disable={true}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"Province License"}
                    maxLength={100}
                    value={this.state.userData.province_licensed}
                    disable={true}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                  <TextInputComponent
                    isSecure={false}
                    value={this.state.userData.driving_licence_no}
                    placeholder={"Driving Licence No."}
                    maxLength={100}
                    disable={true}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"SIN No."}
                    maxLength={100}
                    disable={true}
                    value={this.state.userData.sin_no}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
                <TextInputComponent
                    isSecure={false}
                    placeholder={"Fax"}
                    disable={true}
                    value={this.state.userData.fax}
                    maxLength={100}
                    onChangeText={(text) => this.setState({ confirmPassword: text })}
                    isShowDrawable={false}
                />
              
                </ScrollView>
                </View>

        );
    }
}
const mapStateToProps = state => {
    return {
        userData: state.user.userData
    }
};

const styles = StyleSheet.create({
   
    profileDesc:{
        marginStart:20,
        fontSize:16,
        marginTop:20
    },
    logo: {
        height: 20,
        width: 20,
        marginStart: 20
     },
     home: {
        marginStart: 20,
        fontSize: 24,
        fontWeight: '600'
     },
     image:{
        height:100,
        width:100
     },
     toolbar: {
        backgroundColor: colors.white,
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        
     }
})



export default connect(mapStateToProps, null)(MyProfile);