
import React from 'react';
import { View, Image, Text, StatusBar, TouchableOpacity, Easing, SafeAreaView, StyleSheet, ScrollView, FlatList } from 'react-native';
// import SignInNavigator from '../../navigation/SignInNavigator'
import ToolBarComponent from '../../components/toolbar'
import DatePicker from '../../components/datePicker'
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import colors from '../../utils/colors';
import UploadImage from '../../components/uploadContainer';
import CheckBoxComponent from '../../components/checkbox';
import TextInputComponent from '../../components/textInput'

import * as SSOServices from '../../services/SSOService'
import ModalAlert from '../../utils/modal'
import { connect } from "react-redux";
import { createStackNavigator } from 'react-navigation-stack';
import CalenderView from '../../components/textInput/calenderView';
import DropDownView from '../../components/textInput/dropDown'
import { getDateStringFromDate } from '../../utils';
import RBSheet from "react-native-raw-bottom-sheet";
import moment from 'moment'
import ImagePicker from 'react-native-image-crop-picker';
import reducers from '../../redux/reducers';
import Modal from '../../utils/modal';
const MAX_IMAGE_HEIGHT = 1024
const MAX_IMAGE_WIDTH = 1024
const IMAGE_QUALITY = 1


class cancelpolicy extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            policydata: {
                policy_holder_name: '',
            },
            cancelDate: '',
            uploadPhoto: null,
            isVisa: false,
            showVisa: false,
            visaRefusal: 'Choose File',
            passportImageText: 'Choose File',
            boardingPassText: 'Choose File',
            refusalImage: null,
            passportImage: null,
            boardingPass: null,
            remarks: '',
            status: props.navigation.state.params.status,
            cancelled: false,
            options: 'corrections',
            financial:""

        };

    }


    async componentDidMount() {
        this.getActiveEnsured()
        this.getVoidDocument()
        this.getCancel();
    }


    getVoidDocument = () =>{
        SSOServices.getVoidDocument().then(res=>{

        }).catch(err=>{

        })
    }

    getCancel = () => {
        // let modal = ModalAlert.createProgressModal('Fetching Data...', false)

        let id = this.props.navigation.state.params.id

        SSOServices.getPolicyById(id).then(res => {
            // ModalAlert.hide(modal);
            this.setState({
                policydata: res.data,
                cancelled: res.data.policy_status != 1,
                cancelDate: res.data.cancellation_date != null ? res.data.cancellation_date : ''
            })
        }).catch(err => {
            // ModalAlert.hide(modal)
        })
    }


    _onClickImagePicker = () => {
        ImagePicker.openPicker({
            includeBase64: true,
            compressImageQuality: IMAGE_QUALITY,
            compressImageMaxWidth: MAX_IMAGE_WIDTH,
            compressImageMaxHeight: MAX_IMAGE_HEIGHT,
            mediaType: 'photo',
            cropping: false,
            includeExif: true
        }).then(image => {
            this._validateImage(image)
        }).catch(e => {
            if (Platform.OS === 'ios' && (e.code === 'E_PICKER_NO_CAMERA_PERMISSION' || e.code === 'E_PERMISSION_MISSING')) {
                this._showIosSettingsPopup("You must change the photo access privileges. To do this, go to Settings > Privacy > Photos")
            }
        });

    }

    isValidate = () => {
        if (this.state.cancelDate == '') {
            ModalAlert.error("Please select the Cancellation Date.");
            return false
        }

        if (this.state.remarks == '') {
            ModalAlert.error("Please enter Remarks.");
            return false
        }

        if (this.state.showVisa) {
            if (this.state.passportImage == null) {
                ModalAlert.error("Please select Passport copy.");
                return false
            }

            if (this.state.boardingPass == null) {
                ModalAlert.error("Please select boarding passes copy.");
                return false
            }
        }

        return true;
    }



    _onClickOpenCamera = () => {
        ImagePicker.openCamera({
            includeBase64: true,
            sImageQuality: IMAGE_QUALITY,
            compressImageMaxWidth: MAX_IMAGE_WIDTH,
            compressImageMaxHeight: MAX_IMAGE_HEIGHT,
            mediaType: 'photo',
            cropping: false,
            includeExif: true,
            waitAnimationEnd: false
        }).then(image => {
            this._validateImage(image)
        }).catch(e => {
            if (Platform.OS === 'ios' && (e.code === 'E_PICKER_NO_CAMERA_PERMISSION' || e.code === 'E_PERMISSION_MISSING')) {
                this._showIosSettingsPopup("You must change the camer access privileges. To do this, go to Settings > Privacy > Camera")
            }

            alert(e)

        });
    }

    onPressUpload = (type) => {
        this.RBSheet.close()
        setTimeout(() => {
            switch (type) {
                case 1:
                    this._onClickOpenCamera()
                    break;
                case 2:
                    this._onClickImagePicker()
                    break;
            }
        }, 500)
    }



    _showIosSettingsPopup(msg) {
        let modal = Modal.createModal({
            text: 'Info'
        }, {
            text: msg
        }, false,
            Modal.createPrimaryButton('Settings', () => {
                Modal.hide(modal);
                Linking.openURL('app-settings:');
            })
        );
    }
    _validateImage(image) {

        console.log(image)
        if (this.state.imageStatus === 2) {
            this.setState({
                passportImage: image,
                passportImageText: image.filename
            });
        } else if (this.state.imageStatus === 3) {
            this.setState({
                boardingPass: image,
                boardingPassText: image.filename
            });
        } else {
            this.setState({
                refusalImage: image,
                visaRefusal: image.filename
            });
        }

    }





    // getCancel = () =>{
    //     let modal = ModalAlert.createProgressModal('Fetching Data...', false)
    //     let formData = new FormData();
    //     formData.append("user_id", this.props.userData.user_id);
    //     formData.append("role", this.props.userData.role);
    //             SSOServices.getCancel(formData).then(res => {
    //                 this.setState({
    //                     data:res.data
    //                 })
    //         ModalAlert.hide(modal)
    //     }).catch(err => {
    //         ModalAlert.hide(modal)
    //     })

    // }

    // renderButtons = () => {
    //     return (
    //         <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
    //             <TouchableOpacity onPress={() => { }} activeOpacity={0.7} style={styles.nextButton}>
    //                 <Text style={styles.next}>Search</Text>
    //             </TouchableOpacity>
    //             <TouchableOpacity onPress={() => { }} activeOpacity={0.7} style={styles.nextButton}>
    //                 <Text style={styles.next}>Reset</Text>
    //             </TouchableOpacity>
    //         </View>
    //     )
    // }


    handleDatePicked = (data) => {
        let date = getDateStringFromDate(data);

        this.setState({
            fromDate: false, cancelDate: date
        })

        let date2 = moment(moment().format('YYYY-MM-DD'));
        let date3 = moment(date)


        if (date2.diff(date3, 'days') > 0) {
            this.setState({
                showVisa: true
            })
        } else {
            this.setState({
                showVisa: false
            })
        }

    }


    onPressCamera = (status) => {
        this.setState({
            imageStatus: status
        })

        this.RBSheet.open();
    }


    cancellationApi = () => {

        if (this.isValidate()) {
            let modal = ModalAlert.createProgressModal('Cancelling Please wait...')
            let formData = new FormData();
            formData.append("user_id", this.props.userData.user_id);
            formData.append("policy_id", this.props.navigation.state.params.id);
            formData.append("cancel_date_endorsement", this.state.cancelDate);
            formData.append("cancel_remarks", this.state.remarks);
            if (this.state.showVisa) {
                formData.append("passport_copy ", {
                    uri: this.state.passportImage.path,
                    type: this.state.passportImage.mime,
                    name: 'image',

                })
                formData.append("boarding_passes ", {
                    uri: this.state.boardingPass.path,
                    type: this.state.boardingPass.mime,
                    name: 'image',

                })
            } else {
                // formData.append("passport_copy ", null)
                // formData.append("boarding_passes ", null)  
            }


            SSOServices.cancelPolicy(formData).then(res => {
                ModalAlert.hide()
                ModalAlert.alert(res.message);
            }).catch(err => {
                ModalAlert.hide()
                if (err) {
                    ModalAlert.error(err.message)
                } else {
                    ModalAlert.error('Something went wrong.')
                }
            })
        }
    }

    isValidateNonFinancialFields = () =>{

    }


    financialForm = () =>{

    }


    nonFinancialForm = () => {
        if(this.isValidateNonFinancialFields()){
            
        }
    }

    onPressCancel = () => {
        switch (this.state.options) {
            case 'void':

                break;
            case 'cancellation':
                this.cancellationApi()
                break;
            case 'midTerm':

                break;
            case 'corrections':{

                if(this.state.financial == "fin"){
                    this.financialForm()
                }else{
                    this.nonFinancialForm()
                }


                break;
            }
            default:{
                Modal.error("Please select an option.")
                break;
            }
        }

    }

    getActiveEnsured=()=>{
        
        let id = this.props.navigation.state.params.quotation_id     
        SSOServices.getActiveEnsured(id).then(res=>{

        }).catch(err=>{

        })
    }


    getStatus = (status) => {
        if (status == 2) {
            return "Pending Cancellation";
        } else if (status == 3) {
            return "Cancelled";
        }
    }

    renderCancelOption = () => {
        switch (this.state.options) {
            case 'cancellation': {
                return (
                    <View>
                        <Text style={{ fontSize: 20 }}>Type : <Text style={{ color: 'rgb(62, 185, 186)', fontWeight: '600' }}>Cancellation Backdated</Text></Text>

                        <View style={[styles.singleitem, { justifyContent: 'flex-start' }]}>
                            <Text style={{ fontWeight: '700', marginStart: 20, fontSize: 15, color: 'black' }} > Cancellation Date : </Text>
                            <CalenderView
                                showCalender={true}
                                disabled={this.state.cancelled}
                                style={{ width: '40%', marginTop: -10, marginStart: 25 }}
                                onPress={() => this.setState({ fromDate: true })}
                                title={this.state.cancelDate} />

                        </View>

                        <DatePicker
                            datePicked={(data) => this.handleDatePicked(data, 1)}
                            dateCanceled={() => this.setState({ fromDate: false })}
                            showDate={this.state.fromDate} />
                        <View style={[styles.singleitem, { justifyContent: 'flex-start' }]}>
                            <Text style={{ fontWeight: '700', marginStart: 20, width: '50%', marginStart: 25, fontSize: 15, color: 'black' }}>Remarks *: </Text>
                            <View style={{ width: '40%', marginStart: -40, marginTop: -30 }}>
                                <TextInputComponent
                                    isSecure={false}
                                    placeholder={""}
                                    keyboardType={"default"}
                                    icon={-1}
                                    disable={this.state.cancelled}
                                    styles={{ width: '100%', alignSelf: 'flex-start', alignContent: 'flex-start', justifyContent: 'flex-start', marginStart: 0 }}
                                    value={this.state.otp}
                                    onChangeText={(text) => this.setState({ remarks: text })}
                                    isShowDrawable={false}
                                />
                            </View>

                        </View>
                        {
                            (this.state.showVisa) &&

                            <View>

                                <View style={styles.singleitem}>
                                    <Text style={{ fontWeight: '700', marginStart: 10, width: '50%', marginStart: 10, fontSize: 15, color: 'black' }} > Passport Copy *: </Text>
                                    <TouchableOpacity onPress={() => this.onPressCamera(2)} style={{ width: '35%', borderRadius: 5, borderWidth: 1, marginEnd: 20 }}>
                                        <Text style={{ paddingTop: 10, paddingStart: 10, paddingBottom: 10 }} >{this.state.passportImageText}</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.singleitem}>
                                    <Text style={{ fontWeight: '700', marginStart: 10, width: '50%', fontSize: 15, color: 'black' }} > Boarding Passes Copy *: </Text>
                                    <TouchableOpacity onPress={() => this.onPressCamera(3)} style={{ width: '35%', borderRadius: 5, borderWidth: 1, marginEnd: 20 }}>
                                        <Text style={{ paddingTop: 10, paddingStart: 10, paddingBottom: 10 }} >{this.state.boardingPassText}</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>}
                    </View>
                )
            }
            case 'void': {
                return this.renderVoid();
            }
            case 'midTerm': {
                return this.renderMidterm();
            }
            case 'corrections': {
                return this.renderCorrections();
            }
        }
    }

    renderVoid = () => {
        return (
            <View>

                <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'space-between', width: '90%', alignSelf: 'center' }}>
                    <Text style={{ fontSize: 20 }}>Type : <Text style={{ color: 'rgb(62, 185, 186)', fontWeight: '600' }}>SVVTC</Text></Text>
                    <TouchableOpacity style={{ backgroundColor: 'rgb(62, 185, 186)', paddingStart: 20, paddingEnd: 20, paddingTop: 10, paddingBottom: 10, borderRadius: 10 }}>
                        <Text style={{ color: 'white', fontWeight: '600' }}>Add New</Text>
                    </TouchableOpacity>
                </View>
                <DropDownView
                    styles={{ alignSelf: 'flex-start', marginStart: 10, marginTop: 20 }}
                    childData={[
                        { label: 'User1', value: 'void' },
                        { label: 'User2', value: 'cancellation' }
                    ]}
                    value={this.state.format}
                    onItemSelected={(value) => this.setState({ user: value })}
                    dropDownTitle={"Select User:"} />

                <View>

                    <View style={[styles.singleitem, { justifyContent: 'space-evenly' }]}>
                        <Text style={{ fontWeight: '700', marginStart: 10, width: '50%', marginStart: 10, fontSize: 15, color: 'black' }} >Copy of Passport*: </Text>
                        <TouchableOpacity onPress={() => this.onPressCamera(4)} style={{ width: '35%', borderRadius: 5, borderWidth: 1, marginEnd: 20 }}>
                            <Text style={{ paddingTop: 10, paddingStart: 10, paddingBottom: 10 }} >{this.state.passportImageText}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.singleitem, { justifyContent: 'space-evenly' }]}>
                        <Text style={{ fontWeight: '700', marginStart: 10, width: '50%', fontSize: 15, color: 'black' }}>Visa Rejection Document*: </Text>
                        <TouchableOpacity onPress={() => this.onPressCamera(5)} style={{ width: '35%', borderRadius: 5, borderWidth: 1, marginEnd: 20 }}>
                            <Text style={{ paddingTop: 10, paddingStart: 10, paddingBottom: 10 }} >{this.state.boardingPassText}</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={[styles.singleitem, { justifyContent: 'flex-start' }]}>
                    <Text style={{ fontWeight: '700', marginStart: 20, width: '50%', marginStart: 25, fontSize: 15, color: 'black' }}>Remarks *: </Text>
                    <View style={{ width: '40%', marginTop: -30 }}>
                        <TextInputComponent
                            isSecure={false}
                            placeholder={""}
                            keyboardType={"default"}
                            icon={-1}
                            disable={this.state.cancelled}
                            styles={{ width: '100%', alignSelf: 'flex-start', alignContent: 'flex-start', justifyContent: 'flex-start', marginStart: 0 }}
                            value={this.state.otp}
                            onChangeText={(text) => this.setState({ remarks: text })}
                            isShowDrawable={false}
                        />
                    </View>

                </View>
            </View>
        )
    }



    renderMidterm = () => {
        return (
            <View>
                <Text style={{ marginStart: 20, color: 'red', marginTop: 20 }}>*The minimum premium is $20 per policy.</Text>

                <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'space-between', width: '90%', alignSelf: 'center' }}>
                    <Text style={{ fontSize: 20 }}>Type : <Text style={{ color: 'rgb(62, 185, 186)', fontWeight: '600' }}>Cancellation</Text></Text>
                    <TouchableOpacity style={{ backgroundColor: 'rgb(62, 185, 186)', paddingStart: 20, paddingEnd: 20, paddingTop: 10, paddingBottom: 10, borderRadius: 10 }}>
                        <Text style={{ color: 'white', fontWeight: '600' }}>Add New</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.singleitem, { justifyContent: 'flex-start' }]}>
                    <Text style={{ fontWeight: '700', marginStart: 20, fontSize: 15, color: 'black' }} > Cancellation Date : </Text>
                    <CalenderView
                        showCalender={true}
                        disabled={this.state.cancelled}
                        style={{ width: '40%', marginTop: -10, marginStart: 25 }}
                        onPress={() => this.setState({ fromDate: true })}
                        title={this.state.cancelDate} />

                </View>


                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <DropDownView
                        styles={{ alignSelf: 'flex-start', marginStart: 10, marginTop: 20 }}
                        childData={[
                            { label: 'User1', value: 'void' },
                            { label: 'User2', value: 'cancellation' }
                        ]}
                        value={this.state.format}
                        onItemSelected={(value) => this.setState({ user: value })}
                        dropDownTitle={"Select User:"} />
                    <Text style={{ fontWeight: '600', marginTop: 20, fontSize: 20, marginEnd: 20 }}>Premium Amount{"\n"}$ 1128</Text>
                </View>

                <View style={[styles.singleitem, { justifyContent: 'flex-start' }]}>
                    <Text style={{ fontWeight: '700', marginStart: 20, width: '50%', marginStart: 25, fontSize: 15, color: 'black' }}>Remarks *: </Text>
                    <View style={{ width: '40%', marginTop: -30 }}>
                        <TextInputComponent
                            isSecure={false}
                            placeholder={""}
                            keyboardType={"default"}
                            icon={-1}
                            disable={this.state.cancelled}
                            styles={{ width: '100%', alignSelf: 'flex-start', alignContent: 'flex-start', justifyContent: 'flex-start', marginStart: 0 }}
                            value={this.state.otp}
                            onChangeText={(text) => this.setState({ remarks: text })}
                            isShowDrawable={false}
                        />
                    </View>

                </View>

            </View>
        )
    }

    renderCorrections = () => {
        return (
            <View>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                    <DropDownView
                        styles={{ alignSelf: 'flex-start', marginStart: 10, marginTop: 20 }}
                        childData={[
                            { label: 'Financial Fields', value: "fin" },
                            { label: 'Non Financial Fields', value: "non_fin" }
                        ]}
                        value={this.state.financial}
                        onItemSelected={(value) => this.setState({ financial: value })}
                        dropDownTitle={"Type of corrections:"} />


                    <Text style={{ fontSize: 20, marginTop: 20, marginEnd: 20 }}>Type : <Text style={{ color: 'rgb(62, 185, 186)', fontWeight: '600' }}>Policy Change</Text></Text>

                </View>

                {this.state.financial === "non_fin" && <View>
                    <TextInputComponent
                        isSecure={false}
                        placeholder={"Email"}
                        maxLength={30}
                        value={this.state.nonFinEmail}
                        onChangeText={(text) => this.setState({ nonFinEmail: text })}
                        isShowDrawable={false}
                    />
                    <TextInputComponent
                        isSecure={false}
                        placeholder={"City"}
                        maxLength={30}
                        value={this.state.nonFinCity}
                        onChangeText={(text) => this.setState({ nonFinCity: text })}
                        isShowDrawable={false}
                    />
                    <TextInputComponent
                        isSecure={false}
                        placeholder={"Postal Code"}
                        maxLength={30}
                        value={this.state.nonFinPostCode}
                        onChangeText={(text) => this.setState({ nonFinPostCode: text })}
                        isShowDrawable={false}
                    />
                    <TextInputComponent
                        isSecure={false}
                        placeholder={"Address"}
                        maxLength={30}
                        value={this.state.nonFinAddress}
                        onChangeText={(text) => this.setState({ nonFinAddress: text })}
                        isShowDrawable={false}
                    />
                    <TextInputComponent
                        isSecure={false}
                        placeholder={"Phone"}
                        maxLength={30}
                        value={this.state.nonFinPhone}
                        onChangeText={(text) => this.setState({ nonFinPhone: text })}
                        isShowDrawable={false}
                    />
                    <TextInputComponent
                        isSecure={false}
                        placeholder={"Remarks"}
                        maxLength={30}
                        value={this.state.nonFinRemarks}
                        onChangeText={(text) => this.setState({ nonFinRemarks: text })}
                        isShowDrawable={false}
                    />
                </View>
                }


                {
                    this.state.financial === "fin" &&
                    <View>

                        <Text style={{ fontSize: 20, marginStart: 20, marginTop: 20 }}>Finance Type or Corrections*</Text>
                        <View style={{ flexDirection: 'row', width: '90%',alignSelf:'center',justifyContent:'space-between', marginTop: 20 }}>
                            <TouchableOpacity onPress={() => this.onPressValueRadio(true)} style={{ width: '45%', flexDirection: 'row', marginStart: 10, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                <Image source={this.state.status ? require('../../assets/on.png') : require('../../assets/off.png')} style={{ height: 20, width: 20 }} />
                                <Text style={{ fontSize: 16, marginStart: 5 }}>Correction in insured details</Text>

                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.onPressValueRadio(false)} style={{ width: '45%', marginStart: 10, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                <Image source={!this.state.status ? require('../../assets/on.png') : require('../../assets/off.png')} style={{ height: 20, width: 20 }} />
                                <Text style={{ fontSize: 16, marginStart: 5 }}>Extension of departure date</Text>

                            </TouchableOpacity>
                        </View>

                        <DropDownView
                            styles={{ alignSelf: 'flex-start', width: '100%', marginTop: 20 }}
                            childData={[
                                { label: 'Financial Fields', value: true },
                                { label: 'Non Financial Fields', value: false }
                            ]}
                            value={this.state.financial}
                            onItemSelected={(value) => this.setState({ financial: value })}
                            dropDownTitle={"Name of Insured:"} />

                        <TextInputComponent
                            isSecure={false}
                            placeholder={"Corporation in Name"}
                            maxLength={30}
                            value={this.state.firstName}
                            onChangeText={(text) => this.setState({ firstName: text })}
                            isShowDrawable={false}
                        />
                        <TextInputComponent
                            isSecure={false}
                            placeholder={"DOB"}
                            maxLength={30}
                            value={this.state.firstName}
                            onChangeText={(text) => this.setState({ firstName: text })}
                            isShowDrawable={false}
                        />

                        <View style={styles.singleitem}>
                            <Text style={{ fontWeight: '700', marginStart: 10, width: '50%', marginStart: 10, fontSize: 15, color: 'black' }} >Proof:</Text>
                            <TouchableOpacity onPress={() => this.onPressCamera(2)} style={{ width: '35%', borderRadius: 5, borderWidth: 1, marginEnd: 20 }}>
                                <Text style={{ paddingTop: 10, paddingStart: 10, paddingBottom: 10 }} >{this.state.passportImageText}</Text>
                            </TouchableOpacity>
                        </View>
                        <TextInputComponent
                            isSecure={false}
                            placeholder={"Remarks"}
                            maxLength={30}
                            value={this.state.firstName}
                            onChangeText={(text) => this.setState({ firstName: text })}
                            isShowDrawable={false}
                        />

                    </View>
                }


            </View>
        )
    }

    onPressValueRadio = (value) => {
        this.setState({
            status: value
        })
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <ToolBarComponent
                    title={"Policy Cancellation"}
                    navigation={this.props.navigation} />

                <ScrollView>

                    <View style={styles.listContainer}>

                        <Text style={styles.texthead}>*Minimum charges of Cancellation will be CAD 20</Text>
                    </View >

                    <View style={styles.listContainer}>
                        <View style={styles.singleitem}>
                            <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Policy Holder Name : </Text>
                            <Text style={styles.inputcvv} >{this.state.policydata.policy_holder_name}</Text>
                        </View>


                        <View style={styles.singleitem}>
                            <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Policy Created Date : </Text>
                            <Text style={styles.inputcvv} >{this.state.policydata.first_date_of_cover}</Text>
                        </View>


                        <View style={styles.singleitem}>
                            <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Policy Amount : </Text>
                            <Text style={styles.inputcvv} >{(this.state.policydata.quote_amount) ? 'CAD ' + this.state.policydata.quote_amount : ''}</Text>
                        </View>

                        <View style={styles.singleitem}>
                            <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > First Date of Cover : </Text>
                            <Text style={styles.inputcvv} >{this.state.policydata.first_date_of_cover}</Text>
                        </View>


                        <View style={styles.singleitem}>
                            <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Last Date of cover : </Text>
                            <Text style={styles.inputcvv} >{this.state.policydata.last_date_of_cover}</Text>
                        </View>

                        <View style={styles.singleitem}>
                            <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Duration : </Text>
                            <Text style={styles.inputcvv} >{this.state.policydata.duration}</Text>
                        </View>




                        {/* {this.state.showVisa &&   
                     <View style={[styles.singleitem, { justifyContent: 'flex-start', marginTop: 5 }]}>
                            <Text style={{ fontWeight: '700', marginStart: 10, marginTop: 28, fontSize: 15, color: 'black' }} > Do you have visa ? </Text>
                            <CheckBoxComponent
                                onClickPress={(status) => this.setState({ isVisa: status })}
                                value={this.state.isVisa}
                                isLogin={false}
                                title={""} />
                        </View>} */}
                        {/* 
                        {
                            (this.state.showVisa &&!this.state.isVisa) &&
                            <View style={[styles.singleitem]}>
                                <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Visa Refusal Letter *: </Text>
                                <TouchableOpacity onPress={()=> this.onPressCamera(1)} style={{ width: '100%', marginStart: 20 }}>
                                    <Text style={styles.inputcvv} >{this.state.visaRefusal}</Text>
                                </TouchableOpacity>
                            </View>
                        } */}

                        {/* <View style={[styles.singleitem, { justifyContent: 'flex-start' }]}>
                            <Text style={{ fontWeight: '700', marginStart: 10, width: '50%', marginStart: 10, fontSize: 15, color: 'black' }} > Remarks *: </Text>
                            <View style={{ width: '40%', marginStart: -16, marginTop: -30 }}>
                                <TextInputComponent
                                    isSecure={false}
                                    placeholder={""}
                                    keyboardType={"default"}
                                    icon={-1}
                                    disable={this.state.cancelled}
                                    styles={{ width: '100%', alignSelf: 'flex-start', alignContent: 'flex-start', justifyContent: 'flex-start', marginStart: 0 }}
                                    value={this.state.otp}
                                    onChangeText={(text) => this.setState({ remarks: text })}
                                    isShowDrawable={false}
                                />
                            </View>

                        </View> */}





                        {
                            (this.state.cancelled && this.state.policydata.refund_amount != null) &&
                            <View>
                                <View style={styles.singleitem}>
                                    <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Refund Amount : </Text>
                                    <Text style={styles.inputcvv} >{`CAD ` + this.state.policydata.refund_amount}</Text>
                                </View>

                                <View style={styles.singleitem}>
                                    <Text style={{ fontWeight: '700', marginStart: 10, fontSize: 15, color: 'black' }} > Status : </Text>
                                    <Text style={styles.inputcvv} >{this.getStatus(this.state.policydata.policy_status)}</Text>
                                </View>
                            </View>
                        }





                        {/* 
                        <UploadImage
                            onPress={() => this.onPressCamera(1)}
                            image={this.state.passportImage ? this.state.passportImage.data : null}
                            title={"Upload Passport Copy"}  />
                        <UploadImage
                            onPress={() => this.onPressCamera(2)}
                            image={this.state.boardingPass ? this.state.boardingPass.data : null}
                            title={"Upload Boarding Passes"}  /> */}
                        <View style={{ marginTop: 20 }} />
                    </View>


                    <DropDownView
                        styles={{ alignSelf: 'flex-start', marginStart: 10, marginTop: 10 }}
                        childData={[
                            { label: 'Void', value: 'void' },
                            { label: 'Cancellation', value: 'cancellation' },
                            { label: 'Refund mid-term', value: 'midTerm' },
                            { label: 'Corrections', value: 'corrections' },
                        ]}
                        value={this.state.options}
                        disabled={this.state.disableAll}
                        onItemSelected={(value) => this.setState({ options: value })}
                        dropDownTitle={"Select Options:"} />



                    {
                        this.renderCancelOption()
                    }


                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        height={150}
                        openDuration={250}
                        customStyles={{
                            container: {
                                justifyContent: "center",
                                alignItems: "center"
                            }
                        }}
                    >
                        <View>

                            <TouchableOpacity onPress={() => this.onPressUpload(1)}>
                                <Text style={styles.gallery}>Open Camera</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.onPressUpload(2)}>
                                <Text style={styles.gallery}>Open Gallery</Text>
                            </TouchableOpacity>
                        </View>
                    </RBSheet>
                    {/* <View style={styles.flexDirection}>
                    <DropDownView
                        childData={[
                            { label: 'MGA', value: 'MGA' },
                            { label: 'AGA', value: 'AGA' },
                            { label: 'Advisor', value: 'Advisor' },
                        ]}
                        dropDownTitle={"Select Role:"} />
                    <DropDownView
                        childData={[
                            { label: 'Football', value: 'football' },
                            { label: 'Baseball', value: 'baseball' },
                            { label: 'Hockey', value: 'hockey' },
                        ]}
                        dropDownTitle={"Select User:"} />
                </View>

                <View style={styles.datesContainer}>
                    <View style={{ width: '50%' }}>
                        <Text>From Date</Text>
                        <CalenderView
                            style={{ width: '95%', marginEnd: 20 }}
                            onPress={() => { }}
                            showCalender={true}
                            title={""} />
                    </View>
                    <View style={{ width: '50%' }}>
                        <Text>To Date</Text>
                        <CalenderView
                            style={{ width: '95%', marginEnd: 10 }}
                            showCalender={true}
                            onPress={() => { }}
                            title={""} />
                    </View>
                </View> */}

                    {/* {this.renderButtons()} */}
                    {/* 
                {this.renderTableView()} */}



                    {this.state.status === 1 && <TouchableOpacity onPress={() => this.onPressCancel()} style={{ padding: 10, height: 50, alignItems: 'center', justifyContent: 'center', alignSelf: 'flex-end', marginStart: 10, marginTop: 20, marginRight: 20, width: '45%', borderRadius: 10, backgroundColor: 'rgb(62, 185, 186)' }}>
                        <Text style={{ textAlign: 'center', color: 'white', fontWeight: '800', fontSize: 16 }}>Cancel</Text>
                    </TouchableOpacity>}
                </ScrollView>


            </SafeAreaView>

        );
    }
}
const mapStateToProps = state => {
    return {
        userData: state.user.userData
    }
};

const styles = StyleSheet.create({

    inputcvv: {
        width: '50%'
    },

    container: {

    },
    datesContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignSelf: 'center',
        marginStart: 20,
        marginTop: 20
    },
    listContainer: {
        backgroundColor: colors.white,
        marginTop: 10,
        marginStart: 20,
        marginRight: 20,
        width: '90%',
        paddingTop: 10,
        paddingBottom: 10,
        shadowColor: "#010000",
        shadowOffset: {
            width: 0,
            height: 0
        },
        shadowRadius: 1,
        shadowOpacity: 1,
        elevation: 5
    },
    flexDirection: {
        flexDirection: 'row',
        width: '100%',
        alignSelf: 'center'
    },
    inputWrap: {
        marginLeft: 10,
        marginRight: 10,
    },
    inputWrap1: {
        marginLeft: 10,
        marginRight: 10,
    },
    icon: {
        height: 200,
        width: 200, position: 'absolute',
        alignSelf: 'center',
        top: 100
    },
    singleitem: { flexDirection: 'row', justifyContent: 'space-between', marginTop: 30, },
    paragraph: {
        marginTop: 0,
        fontSize: 24,
        color: 'black',
        fontWeight: '600'
    },
    gallery: {
        fontSize: 20,
        marginBottom: 20,
        padding: 5
    },
    texthead: {
        textAlign: 'center',
        marginTop: 0,
        fontSize: 14,
        color: 'red',
    },

    logo: {
        height: '100%',
        width: '100%'
    },
    nextButton: {
        backgroundColor: '#3F6183',
        width: 140,
        marginTop: 20,
        height: 45,
        marginStart: 10,
        marginEnd: 10,
        alignSelf: 'center',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    next: {
        color: colors.white,
        fontSize: 18,
        fontWeight: '600'
    },


    tableView: {
        backgroundColor: colors.primary,
        marginStart: 20, marginEnd: 20,
        flexDirection: 'row',
        marginTop: 20,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    itemText: {
        color: colors.white,
        width: 160,
        padding: 20,
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        fontSize: 16
    },
    itemView: {
        flexDirection: 'row',
        borderWidth: 1,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginStart: 20, marginEnd: 20,
    },
    listItemText: {
        width: 160,
        alignContent: 'center',
        alignItems: 'center',
        padding: 20,
        textAlign: 'center',
        fontSize: 16
    },
})



export default connect(mapStateToProps, null)(cancelpolicy);